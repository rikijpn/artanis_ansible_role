# ansible artanis install role
Install artanis with guile-dbi (database support) with guile-dbd-mysql on debian systems using ansible.

## The end result
1. a binary with "art" (artanis)
2. artanis' and guile-dbi's libraries
3. some paths in your ~/.profile to run art in the future from the command line (might need to do `source ~/.profile` before trying to run artanis if you are not using a login shell.

## Getting Started
You might want to change the variables `artanis_sources_path_root` or `artanis_install_path` in your playbook depending on your preferences.  
By default the install root path is ~/bin/. So your `art` binary will be in ~/bin/bin.

### Prerequisites
- A debian system (tested on debian 9)
- Ansible.
- guile installed (I have another role for that)

### How to run
If you're just installing in a local path (on your home dir), then

```
ansible-playbook your_playbook_including_this_role.yml
```
will do.  
If you are installing artanis to a system path (that would require root access), then add "-b" at the end.


### So it's installed, how do I test/validate it?
```
# type "art help" in the command line, it should look like:
$ art help

GNU Artanis is a monolithic framework written in Guile Scheme.
NalaGinrut <mulei@gnu.org>

commands:
create              Create a new Artanis project.
draw                Generate component automatically, say, MVC.
help                Show this screen
migrate             DB migration tools.
version             Show the version info
work                Run the server and make the site/app work.

GPLv3+ & LGPLv3+
Version: GNU Artanis-0.2.5.
God bless hacking.

```

For actual sample code you are free to look at my repo's artanis sample website, or check artanis' official docs to write a quick thing or test the art command works.


# Motivation
Artanis isn't the easiest thing to install...

# This is not github did you notice?
Yeah, totally. Because, f*ck microsoft.  
I think someday ansible galaxy will accept other repos besides github. Until then, just clone this repo and use it directly (can't use ansible galaxy).
